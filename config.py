import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
	"""Application Configuration """

	SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard to guess string'
	MAIL_SERVER = os.environ.get('MAIL_SERVER', 'smtp.googlemail.com')
	MAIL_PORT = int(os.environ.get('MAIL_PORT', '587'))
	MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS', 'true').lower() in \
       ['true', 'on', '1']
	MAIL_USERNAME = os.environ.get('MAIL_USERNAME') or 'takni2009@gmail.com'
	MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD') or '<password>'
	FLASKY_MAIL_SUBJECT_PREFIX = '[Shops]'
	FLASKY_MAIL_SENDER = 'takni2009@gmail.com'
	FLASKY_ADMIN = os.environ.get('SHOPS_ADMIN') or 'takni2009@gmail.com'
	# SSL_REDIRECT = False
	SQLALCHEMY_TRACK_MODIFICATIONS = False

	SQLALCHEMY_COMMIT_ON_TEARDOWN = True

	@staticmethod
	def init_app(app):
		pass

class DevelopmentConfig(Config):
	"""Development Configuration """
	
	DEBUG = True
	SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data-dev.sqlite')

class TestingConfig(Config):
	"""Testing Configuration"""
	
	TESTING = True
	SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data-test.sqlite')
	WTF_CSRF_ENABLED = False

class ProductionConfig(Config):
	"""Production Configuration"""
	
	SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data.sqlite')

config = {
	'development' : DevelopmentConfig,
	'testing' : TestingConfig,
	'production' : ProductionConfig,
	'default' : DevelopmentConfig
}